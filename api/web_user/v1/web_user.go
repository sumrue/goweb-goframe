package v1

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// RegisterReq 用户注册
type RegisterReq struct {
	g.Meta      `path:"/register" method:"post" sm:"web用户注册"`
	UserName    string `json:"user_name" sm:"用户名" v:"required"`
	Password    string `json:"password" sm:"密码" v:"required"`
	NikeName    string `json:"nike_name" sm:"用户昵称" v:"required"`
	PhoneNumber string `json:"phone_number" sm:"用户手机号" v:"required"`
	Email       string `json:"email" sm:"邮箱"`
}

type RegisterRes struct {
	g.Meta `mime:"text/html" example:"string"`
}

// LoginReq 用户登录
type LoginReq struct {
	g.Meta      `path:"login" method:"get" sm:"web用户登录"`
	PhoneNumber string `json:"phone_number" sm:"登录手机号" v:"required"`
	Password    string `json:"password" sm:"密码" v:"required"`
}
type LoginRes struct {
	Token string `json:"token"`
}

type WebUserEditReq struct {
	g.Meta       `path:"/webuseredit" method:"post" sm:"用户资料编辑"`
	Id           string `json:"id" v:"required"` // id
	NikeName     string `json:"nike_name"`       // 用户昵称
	HeadPortrait string `json:"head_portrait"`   // 用户头像地址
	Email        string `json:"email"`           // 邮箱
}

type WebUserEditRes struct {
	g.Meta `mime:"text/html" example:"string"`
}

type UserInfoReq struct {
	g.Meta `path:"/userinfo" method:"get" sm:"获取用户信息"`
}
type UserInfoRes struct {
	Id                   string      `json:"id"`                    // id
	UserName             string      `json:"user_name"`             // 用户名
	NikeName             string      `json:"nike_name"`             // 用户昵称
	HeadPortrait         string      `json:"head_portrait"`         // 用户头像地址
	PhoneNumber          string      `json:"phone_number"`          // 用户手机号
	Status               int         `json:"status"`                // 用户状态 0:冻结  1:正常
	Email                string      `json:"email"`                 // 邮箱
	AuthenticationStatus int         `json:"authentication_status"` // 认证状态 0：未认证  1：已认证
	LoginTime            *gtime.Time `json:"login_time"`            // 上次登陆时间
	LoginIp              string      `json:"login_ip"`              // 上次登陆ip
	UserLevel            int         `json:"user_level"`            // 用户等级
	PraiseNum            int         `json:"praise_num"`            // 获得的点赞数
}
