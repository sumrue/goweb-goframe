package main

import (
	_ "github.com/gogf/gf/contrib/drivers/mysql/v2"
	"github.com/gogf/gf/v2/os/gctx"
	"goweb-goframe/internal/cmd"
	_ "goweb-goframe/internal/logic"
	_ "goweb-goframe/internal/packed"
)

func main() {
	cmd.Main.Run(gctx.GetInitCtx())
}
