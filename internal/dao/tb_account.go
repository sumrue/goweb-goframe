// =================================================================================
// This is auto-generated by GoFrame CLI tool only once. Fill this file as you wish.
// =================================================================================

package dao

import (
	"goweb-goframe/internal/dao/internal"
)

// internalTbAccountDao is internal type for wrapping internal DAO implements.
type internalTbAccountDao = *internal.TbAccountDao

// tbAccountDao is the data access object for table tb_account.
// You can define custom methods on it to extend its functionality as you wish.
type tbAccountDao struct {
	internalTbAccountDao
}

var (
	// TbAccount is globally public accessible object for table tb_account operations.
	TbAccount = tbAccountDao{
		internal.NewTbAccountDao(),
	}
)

// Fill with you ideas below.
