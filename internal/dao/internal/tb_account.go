// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// TbAccountDao is the data access object for table tb_account.
type TbAccountDao struct {
	table   string           // table is the underlying table name of the DAO.
	group   string           // group is the database configuration group name of current DAO.
	columns TbAccountColumns // columns contains all the column names of Table for convenient usage.
}

// TbAccountColumns defines and stores column names for table tb_account.
type TbAccountColumns struct {
	Id                   string // id
	UserName             string // 用户名
	Password             string // 密码
	NikeName             string // 用户昵称
	HeadPortrait         string // 用户头像地址
	PhoneNumber          string // 用户手机号
	Status               string // 用户状态 0:冻结  1:正常
	Email                string // 邮箱
	AuthenticationStatus string // 认证状态 0：未认证  1：已认证
	LoginTime            string // 上次登陆时间
	LoginIp              string // 上次登陆ip
	CreateTime           string // 创建时间
	Salt                 string // 加盐
	Base                 string // 用户类型 0：普通  1：可开店铺的用户
	UserLevel            string // 用户等级
	PraiseNum            string // 获得的点赞数
}

// tbAccountColumns holds the columns for table tb_account.
var tbAccountColumns = TbAccountColumns{
	Id:                   "id",
	UserName:             "user_name",
	Password:             "password",
	NikeName:             "nike_name",
	HeadPortrait:         "head_portrait",
	PhoneNumber:          "phone_number",
	Status:               "status",
	Email:                "email",
	AuthenticationStatus: "authentication_status",
	LoginTime:            "login_time",
	LoginIp:              "login_ip",
	CreateTime:           "create_time",
	Salt:                 "salt",
	Base:                 "base",
	UserLevel:            "user_level",
	PraiseNum:            "praise_num",
}

// NewTbAccountDao creates and returns a new DAO object for table data access.
func NewTbAccountDao() *TbAccountDao {
	return &TbAccountDao{
		group:   "default",
		table:   "tb_account",
		columns: tbAccountColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *TbAccountDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *TbAccountDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *TbAccountDao) Columns() TbAccountColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *TbAccountDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *TbAccountDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *TbAccountDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
