// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// SystemCompany is the golang structure of table system_company for DAO operations like Where/Data.
type SystemCompany struct {
	g.Meta               `orm:"table:system_company, do:true"`
	Id                   interface{} // 企业id
	CompanyName          interface{} // 企业名称
	ShortName            interface{} // 企业简称
	Password             interface{} // 密码
	PhoneNumber          interface{} // 绑定手机号
	Logo                 interface{} // 企业logo地址
	Status               interface{} // 账户状态  0:冻结  1:启用
	LoginTime            *gtime.Time // 上次登陆时间
	LoginIp              interface{} // 上次登陆ip
	CreateTime           *gtime.Time // 创建时间
	Salt                 interface{} // 加盐
	Address              interface{} // 详细地址
	Email                interface{} // 邮箱
	AuthenticationStatus interface{} // 认证状态 0:未认证，1:已认证
}
