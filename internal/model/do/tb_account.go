// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// TbAccount is the golang structure of table tb_account for DAO operations like Where/Data.
type TbAccount struct {
	g.Meta               `orm:"table:tb_account, do:true"`
	Id                   interface{} // id
	UserName             interface{} // 用户名
	Password             interface{} // 密码
	NikeName             interface{} // 用户昵称
	HeadPortrait         interface{} // 用户头像地址
	PhoneNumber          interface{} // 用户手机号
	Status               interface{} // 用户状态 0:冻结  1:正常
	Email                interface{} // 邮箱
	AuthenticationStatus interface{} // 认证状态 0：未认证  1：已认证
	LoginTime            *gtime.Time // 上次登陆时间
	LoginIp              interface{} // 上次登陆ip
	CreateTime           *gtime.Time // 创建时间
	Salt                 interface{} // 加盐
	Base                 interface{} // 用户类型 0：普通  1：可开店铺的用户
	UserLevel            interface{} // 用户等级
	PraiseNum            interface{} // 获得的点赞数
}
