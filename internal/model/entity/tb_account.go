// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// TbAccount is the golang structure for table tb_account.
type TbAccount struct {
	Id                   string      `json:"id"                    ` // id
	UserName             string      `json:"user_name"             ` // 用户名
	Password             string      `json:"password"              ` // 密码
	NikeName             string      `json:"nike_name"             ` // 用户昵称
	HeadPortrait         string      `json:"head_portrait"         ` // 用户头像地址
	PhoneNumber          string      `json:"phone_number"          ` // 用户手机号
	Status               int         `json:"status"                ` // 用户状态 0:冻结  1:正常
	Email                string      `json:"email"                 ` // 邮箱
	AuthenticationStatus int         `json:"authentication_status" ` // 认证状态 0：未认证  1：已认证
	LoginTime            *gtime.Time `json:"login_time"            ` // 上次登陆时间
	LoginIp              string      `json:"login_ip"              ` // 上次登陆ip
	CreateTime           *gtime.Time `json:"create_time"           ` // 创建时间
	Salt                 string      `json:"salt"                  ` // 加盐
	Base                 int         `json:"base"                  ` // 用户类型 0：普通  1：可开店铺的用户
	UserLevel            int         `json:"user_level"            ` // 用户等级
	PraiseNum            int         `json:"praise_num"            ` // 获得的点赞数
}
