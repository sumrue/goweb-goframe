// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// SystemCompany is the golang structure for table system_company.
type SystemCompany struct {
	Id                   string      `json:"id"                    ` // 企业id
	CompanyName          string      `json:"company_name"          ` // 企业名称
	ShortName            string      `json:"short_name"            ` // 企业简称
	Password             string      `json:"password"              ` // 密码
	PhoneNumber          string      `json:"phone_number"          ` // 绑定手机号
	Logo                 string      `json:"logo"                  ` // 企业logo地址
	Status               int         `json:"status"                ` // 账户状态  0:冻结  1:启用
	LoginTime            *gtime.Time `json:"login_time"            ` // 上次登陆时间
	LoginIp              string      `json:"login_ip"              ` // 上次登陆ip
	CreateTime           *gtime.Time `json:"create_time"           ` // 创建时间
	Salt                 string      `json:"salt"                  ` // 加盐
	Address              string      `json:"address"               ` // 详细地址
	Email                string      `json:"email"                 ` // 邮箱
	AuthenticationStatus int         `json:"authentication_status" ` // 认证状态 0:未认证，1:已认证
}
