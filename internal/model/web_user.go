package model

type WebUserRegisterReq struct {
	UserName    string `json:"user_name" sm:"用户名"`
	Password    string `json:"password" sm:"密码"`
	NikeName    string `json:"nike_name" sm:"用户昵称"`
	PhoneNumber string `json:"phone_number" sm:"用户手机号"`
	Email       string `json:"email" sm:"邮箱"`
}

type WebUserLoginReq struct {
	PhoneNumber string `json:"phone_number" sm:"登录手机号" v:"required"`
	Password    string `json:"password" sm:"密码" v:"required"`
}

type WebUserEditReq struct {
	Id           string `json:"id" v:"required"` // id
	NikeName     string `json:"nike_name"`       // 用户昵称
	HeadPortrait string `json:"head_portrait"`   // 用户头像地址
	Email        string `json:"email"`           // 邮箱
}
