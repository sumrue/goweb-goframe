package webuser

import (
	"context"
	v1 "goweb-goframe/api/web_user/v1"
	"goweb-goframe/internal/model/entity"
	"goweb-goframe/internal/packed"
)

// UserInfo 获取用户信息
func (s sWebUser) UserInfo(ctx context.Context) (res *v1.UserInfoRes, err error) {
	// 从token中获取用户id
	var TbAccount entity.TbAccount
	if TbAccount, err = packed.GetUserInfo(ctx); err != nil {
		return nil, err
	}
	// 返回查询到的用户信息
	return &v1.UserInfoRes{
		Id:                   TbAccount.Id,
		UserName:             TbAccount.UserName,
		NikeName:             TbAccount.NikeName,
		HeadPortrait:         TbAccount.HeadPortrait,
		PhoneNumber:          TbAccount.PhoneNumber,
		Status:               TbAccount.Status,
		Email:                TbAccount.Email,
		AuthenticationStatus: TbAccount.AuthenticationStatus,
		LoginTime:            TbAccount.LoginTime,
		LoginIp:              TbAccount.LoginIp,
		UserLevel:            TbAccount.UserLevel,
		PraiseNum:            TbAccount.PraiseNum,
	}, nil
}
