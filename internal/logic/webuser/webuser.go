package webuser

import (
	"goweb-goframe/internal/model/entity"
	"goweb-goframe/internal/service"

	"github.com/gogf/gf/v2/crypto/gmd5"
	"github.com/gogf/gf/v2/util/grand"
)

type sWebUser struct {
}

func init() {
	service.RegisterWebUser(&sWebUser{})
}

// GenSalt 生成32位盐值
func genSalt() string {
	return grand.S(32, false)
}

// EncryptPass 加密密码
func encryptPass(pass string, salt string) (encrypt string) {
	encrypt, _ = gmd5.EncryptString(pass + salt)
	return
}

// ValidPass 校验密码
func ValidPass(pass string, TbAccount *entity.TbAccount) bool {
	return TbAccount.Password == encryptPass(pass, TbAccount.Salt)
}
