package webuser

import (
	"context"
	"github.com/gogf/gf/v2/frame/g"
	"goweb-goframe/internal/dao"
	"goweb-goframe/internal/model"
	"goweb-goframe/internal/packed"
)

// Edit 更改用户信息
func (s sWebUser) Edit(ctx context.Context, req *model.WebUserEditReq) (err error) {
	db := dao.TbAccount.Ctx(ctx)
	_, err = db.Data(g.Map{
		"nike_name":     req.NikeName,
		"email":         req.Email,
		"head_portrait": req.HeadPortrait,
	}).Where("id", req.Id).Update()
	if err != nil {
		err = packed.Err.Skip(20105, "用户信息修改错误")
	}
	return
}
