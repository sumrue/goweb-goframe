package cmd

import (
	"context"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/net/ghttp"
	"github.com/gogf/gf/v2/os/gcmd"
	"goweb-goframe/internal/controller/webuser"
	"goweb-goframe/internal/service"
)

var (
	Main = gcmd.Command{
		Name:  "main",
		Usage: "main",
		Brief: "start http server",
		Func: func(ctx context.Context, parser *gcmd.Parser) (err error) {
			s := g.Server()
			s.Group("/", func(group *ghttp.RouterGroup) {
				s.Group("/api/user", func(group *ghttp.RouterGroup) {
					group.Middleware(service.Middleware().Response) // 响应中间件
					//group.Middleware(service.Middleware().Auth)
					group.Bind(
						webuser.NewV1(),
					)
				})
			})
			s.Run()
			return nil
		},
	}
)
