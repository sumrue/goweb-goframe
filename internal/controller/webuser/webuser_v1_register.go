package webuser

import (
	"context"
	"goweb-goframe/internal/model"
	"goweb-goframe/internal/service"

	v1 "goweb-goframe/api/web_user/v1"
)

// 用户注册
func (c *ControllerV1) Register(ctx context.Context, req *v1.RegisterReq) (res *v1.RegisterRes, err error) {
	registerData := &model.WebUserRegisterReq{
		UserName:    req.UserName,
		Password:    req.Password,
		NikeName:    req.NikeName,
		PhoneNumber: req.PhoneNumber,
		Email:       req.Email,
	}
	_, err = service.WebUser().Register(ctx, registerData)
	return
}
