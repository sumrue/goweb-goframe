package webuser

import (
	"context"
	v1 "goweb-goframe/api/web_user/v1"
	"goweb-goframe/internal/service"
)

func (c *ControllerV1) UserInfo(ctx context.Context, req *v1.UserInfoReq) (res *v1.UserInfoRes, err error) {
	res, err = service.WebUser().UserInfo(ctx)
	return
}
