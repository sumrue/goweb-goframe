package webuser

import (
	"context"
	v1 "goweb-goframe/api/web_user/v1"
	"goweb-goframe/internal/model"
	"goweb-goframe/internal/service"
)

func (c *ControllerV1) Login(ctx context.Context, req *v1.LoginReq) (res *v1.LoginRes, err error) {

	reqData := &model.WebUserLoginReq{
		PhoneNumber: req.PhoneNumber,
		Password:    req.Password,
	}
	var token string
	token, err = service.WebUser().Login(ctx, reqData)
	if err == nil {
		res = &v1.LoginRes{
			Token: token,
		}
	}
	return
}
