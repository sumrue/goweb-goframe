package webuser

import (
	"context"
	"goweb-goframe/internal/model"
	"goweb-goframe/internal/service"

	"goweb-goframe/api/web_user/v1"
)

func (c *ControllerV1) WebUserEdit(ctx context.Context, req *v1.WebUserEditReq) (res *v1.WebUserEditRes, err error) {
	reqData := &model.WebUserEditReq{
		Id:           req.Id,
		Email:        req.Email,
		HeadPortrait: req.HeadPortrait,
		NikeName:     req.NikeName,
	}
	err = service.WebUser().Edit(ctx, reqData)
	return
}
