package service

import (
	"context"
	v1 "goweb-goframe/api/web_user/v1"
	"goweb-goframe/internal/model"
)

type IWebUser interface {
	Register(ctx context.Context, req *model.WebUserRegisterReq) (res string, err error)
	Login(ctx context.Context, req *model.WebUserLoginReq) (res string, err error)
	Edit(ctx context.Context, req *model.WebUserEditReq) (err error)
	UserInfo(ctx context.Context) (res *v1.UserInfoRes, err error)
}

var (
	localWebUer IWebUser
)

func WebUser() IWebUser {
	if localWebUer == nil {
		panic("implement not found for interface IAccount, forgot register?")
	}
	return localWebUer
}

func RegisterWebUser(i IWebUser) {
	localWebUer = i
}
