package packed

import (
	"context"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/golang-jwt/jwt/v4"
	"goweb-goframe/internal/consts"
	"goweb-goframe/internal/dao"
	"goweb-goframe/internal/model/entity"
	"time"
)

// 配置文件中获取设置key
var jwtKey = consts.JwtKey

type MyClaims struct {
	Id                   string `json:"id"`
	jwt.RegisteredClaims        // 注意!这是jwt-go的v4版本新增的，原先是jwt.StandardClaims
}

// MakeToken 传入用户信息，返回token
func MakeToken(id string) (tokenString string, err error) {
	claim := MyClaims{
		Id: id, // 要在token保存的用户信息
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(1 * time.Hour * time.Duration(1))), // 过期时间3小时
			IssuedAt:  jwt.NewNumericDate(time.Now()),                                       // 签发时间
			NotBefore: jwt.NewNumericDate(time.Now()),                                       // 生效时间
		}}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim) // 使用HS256算法
	tokenString, err = token.SignedString(jwtKey)             // 加密获取token
	return tokenString, err
}

// GetUserInfo 在token中获取保存的用户信息
func GetUserInfo(ctx context.Context) (TbAccount entity.TbAccount, err error) {
	tokenString := g.RequestFromCtx(ctx).Request.Header.Get("Authorization")
	token, _ := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	var id string
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		id = claims["id"].(string)
	} else {
		err = Err.Skip(20105, "token错误")
		return TbAccount, err
	}

	// 根据id查询用户信息
	db := dao.TbAccount.Ctx(ctx)
	if err = db.Where(id).Scan(&TbAccount); err != nil {
		err = Err.Skip(20105, "查询错误")
		return
	}
	return TbAccount, nil
}
